# @logixboard/eslint-config-typescript

Common eslint-config rules across our typescript packages.

To use this eslint config, do the following:

1. Specify it as a devDependency in package.json of your package you want to lint.
1. Add this package's peer dependencies to devDepencies in package.json of your package you want to lint.
1. Extend your eslint config with the ruleset "@logixboard/eslint-config-typescript"
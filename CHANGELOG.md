# Changelog

## 5.0.0 -> 5.1.0

Add @typescript-eslint/await-thenable rule.

## 4.2.0 -> 5.0.0

Update rules to be able to use prettier as formatter.

## 4.1.0 -> 4.2.0

Update peer dependencies.

## 4.0.0 -> 4.1.0

Update eqeqeq config to always enforce strict equality except when comparing with the null literal.

## 3.2.0 -> 4.0.0

Reenable removed rules from version 3.2.0. Split rules to be easier to find those in eslint. Enable formatting rules.

## 3.1.0 -> 3.2.0

Remove the following rules to be able to reenable prettier. This way prettier is the one that is going to validate those rules instead of eslint:

- arrow-parens
- comma-dangle
- max-len
- no-tabs
- quotes
- quote-props
- semi

## 3.0.1 -> 3.1.0

Updated dependencies to support typescript/eslint versions more compatible with the latest typescript/react.
`"@typescript-eslint/parser": "^5.4.0"` -> `"@typescript-eslint/parser": "^5.10.1"`
`"@typescript-eslint/eslint-plugin": "^5.4.0"` -> `"@typescript-eslint/eslint-plugin": "^5.10.1"`
`"eslint": "^8.3.0"` => `"eslint": "^8.7.0"`
`"typescript": "^4.1.3"` => `"eslint": "^4.5.0"`

## 3.0.0 -> 3.0.1

Updated `unused-imports/no-unused-vars` rule to

- allow unused variables if they start with `_`. This is useful in the `bin` folder scripts, where things may be commented out at various times. (You can also disable this rule there.) This is also required for any functions that may be unreferenced.
- be more lax when destructuring an object if an unused variable is only used to create an object without that property.

Example:

```js
const obj = {
    prop1: 'aslkjdf',
    prop2: 'alskjdfa',
    propToDelete: {
        morestuff: 234
    } || null
};

// `propToDelete` is unused, and the old rule settings would cause an error.
// now, because we're compiling all the rest of the object into a variable called `rest`,
// the new settings will ignore `propToDelete` not being used.
const {propToDelete, ...rest } = obj;

console.log(rest)
/*
    {
        "prop1": "aslkjdf",
        "prop2": "alskjdfa"
    }
*/

// the original object stays unaffected, but be aware that it's still only a shallow copy!
console.log(obj)
/*
    {
        "prop1": "aslkjdf",
        "prop2": "alskjdfa",
        "propToDelete": {
            "morestuff": 234
        }
    }
*/

## 2.0.3 -> 3.0.0
Updated dependencies to get the new eslint rules that have become available over the years.

Removed "prettier" utilities. Some of our dependencies [were merged into "prettier"](https://github.com/prettier/eslint-config-prettier/blob/main/CHANGELOG.md#version-800-2021-02-21), and it now seems a chore to update existing projects if we continue using prettier. TLDR, it was recommending hoisting code that was purposely placed on new lines so it could hit it's "preferred" line length of 80 chars. You can configure line length, but in any case it would require a lot of updates to the existing code bases.

Old code (I'm not claiming either of these is good, just demonstrating before & after!):
```

const result = (await grabAValueFromAnApi(
    'a string value',
    343,
)).doSomething()

```
would become:
```

const result = (await grabAValueFromAnApi('a string value', 343)).doSomething()

```

### Actions needed:

#### Upgrade this package, probably in the root of the repo:
```sh
yarn upgrade @logixboard/eslint-config-typescript -D -W -L
```

> These flags will be used quite a bit in the next steps:
>
> - `-D` means add it as a developer dependency
> - `-W` means do this operation in the yarn root workspace
> - `-L` means upgrade to the latest version of this package. You may want to specify a specific version of dependencies in the future, as this disables dependencies of other packages when determining what version of the package to install.

#### Remove old dependencies that are no longer needed

These deps were removed, as `prettier` is no longer used.

```sh
yarn remove prettier/@typescript-eslint eslint-plugin-prettier eslint-config-prettier prettier -W
```

#### Upgrade dependencies

Upgrade these, specific version may be needed later. If you don't have these in your project, use `yarn add -W -D <package list>` to add them. Review the required peer dependencies list for more info.

You may need to update more or less of these, depending on your project's configs. `eslint-plugin-jsdoc` is recommended because of `farmers-market`, for example.

```sh
yarn upgrade -W -L -D eslint @typescript-eslint/eslint-plugin eslint-plugin-jsdoc @typescript-eslint/parser
```

### New Rules

There may be more depending on the `recommended` plugins that were updated. Please update this list as you find them.

- `no-extra-semi` is now [`eslint-disable-next-line @typescript-eslint/no-extra-semi`](https://github.com/typescript-eslint/typescript-eslint/blob/main/packages/eslint-plugin/docs/rules/no-extra-semi.md) - you may  have to update your 'disable' comments.

> This rule extends the base eslint/no-extra-semi rule. It adds support for class properties.

- [`@typescript-eslint/no-misused-promises`](https://github.com/typescript-eslint/typescript-eslint/blob/main/packages/eslint-plugin/docs/rules/no-misused-promises.md) - avoid using promises in places not designed to handle them.
- [`@typescript-eslint/no-floating-promises`](https://github.com/typescript-eslint/typescript-eslint/blob/main/packages/eslint-plugin/docs/rules/no-floating-promises.md) - requires Promise-like values to be handled appropriately. A promise must either be `await`ed, returned, or called with `void` if you don't care to wait for it to finish executing (`void fireAndForgetThisPromise()`)
- [`quotes`](https://github.com/typescript-eslint/typescript-eslint/blob/main/packages/eslint-plugin/docs/rules/quotes.md) are set to `single`, but `backtick` template literals are allowed (for now?).
- [`arrow-parens`](https://eslint.org/docs/rules/arrow-parens) - backward-compatible with prettier, there should be no change in functionality.

> Lambdas (arrow functions) do not require parentheses around args unless they are required. `const x = y => doSomething(y)` does not require parens. `const x = (y: string) => doSomething(y)` requires parens because the type is specified, e.g.

- [`@typescript-eslint/no-unnecessary-type-constraint`](https://github.com/typescript-eslint/typescript-eslint/blob/main/packages/eslint-plugin/docs/rules/no-unnecessary-type-constraint.md) - points out unnecessary type constraints that may be excessive / repetitive.

> `function myFunc<T extends unknown>(arg: T)` is excessive and unnecessary. Rewrite it as `function myFunc<T>(arg: T)`

- [`@typescript-eslint/explicit-module-boundary-types`](https://github.com/typescript-eslint/typescript-eslint/blob/main/packages/eslint-plugin/docs/rules/explicit-module-boundary-types.md) - changed the options to allow explicit `any` types on parameters and returns. However, consider using generics so the caller knows that the type was or was not modified somehow?
- `@typescript-eslint/no-unused-vars` - disabled, see the next bullet point
- [`unused-imports/no-unused-vars`](https://github.com/sweepline/eslint-plugin-unused-imports) - This takes the place of `@typescript-eslint/no-unused-vars`, since the latter included both variables and imports. The old setup caused one issue to be reported twice with different errors.

> Update any rules that disable `@typescript-eslint/no-unused-vars` to now disable `unused-imports/no-unused-vars`
> The config was also updated so you can [optionally prefix unused arguments](https://github.com/typescript-eslint/typescript-eslint/blob/main/packages/eslint-plugin/docs/rules/no-unused-vars.md) of a method signature with `_` to get around the `no-unused-vars` rule.

## 2.0.0 -> 2.0.3

Some breaking config changes were made between these versions. Some of those that are known are listed here.

- [`@typescript-eslint/explicit-module-boundary-types`](https://github.com/typescript-eslint/typescript-eslint/blob/main/packages/eslint-plugin/docs/rules/explicit-module-boundary-types.md) - explicit types for function return values and arguments makes it clear to any calling code what is the module boundary's input and output.
- [`unused-imports/no-unused-imports`](https://github.com/sweepline/eslint-plugin-unused-imports) - don't import something if you aren't going to use it! This separated out variable declarations vs imported items.
- [`@typescript-eslint/no-unused-vars`](https://github.com/typescript-eslint/typescript-eslint/blob/main/packages/eslint-plugin/docs/rules/no-unused-vars.md) - a variable or parameter could not be declared if it was unused. Note that in v3 you can prefix a function argument with `_` to have the full signature available, if desired.

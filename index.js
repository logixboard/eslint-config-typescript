const possibleProblemsRules = {
    "no-unmodified-loop-condition": "error",
};

// the "unused-imports" plugin separates imports from vars.
const unusedImportsRules = {
    // turn this off, as it captures imports and vars at the same time.
    "@typescript-eslint/no-unused-vars": "off",
    "unused-imports/no-unused-imports": "error",
    "unused-imports/no-unused-vars": [
        "error",
        {
            // optionally preface unused args with underscore
            //  makes it easier to read the full signature
            argsIgnorePattern: "^_",
            // if destructuring object properties where some are unused
            //  and the rest are combined into a single object,
            // the unused vars are ignored.
            // E.g., `const { x, ...rest } = { x: 2343, a: 'a', z: 23 }`
            //  would ignore the var x, as properties `a` and `z`
            //  were bundeled into an object called `rest` (name doesn't matter)
            ignoreRestSiblings: true,
            // vars cannot be unused at all
            vars: "all",
            // except when they start with `_`.
            // mainly useful in our script files where things
            //  can be all sorts of commented-out.
            varsIgnorePattern: "^_",
            // unused args will be an error if they are after, not before,
            // used args.
            args: "after-used",
        },
    ],
};

const typescriptEslintRules = {
    "@typescript-eslint/array-type": [
        "error",
        {
            default: "array",
        },
    ], // Requires using either `T[]` or `Array<T>` for arrays
    "@typescript-eslint/await-thenable": "error", // Requires expressions of type `Promise` to be handled appropriately
    "@typescript-eslint/consistent-type-imports": "error", // Enforces consistent usage of type imports
    "@typescript-eslint/explicit-module-boundary-types": [
        "error",
        { allowArgumentsExplicitlyTypedAsAny: true },
    ], // Require explicit return types on functions and class methods
    "@typescript-eslint/member-ordering": "error", // Require a consistent member declaration order
    "@typescript-eslint/naming-convention": [
        "error",
        {
            selector: "typeParameter",
            format: ["PascalCase"],
            prefix: ["T"],
        },
        {
            selector: "memberLike",
            modifiers: ["private"],
            format: ["camelCase"],
            leadingUnderscore: "require",
        },
    ], // Enforces naming conventions for everything across a codebase
    "@typescript-eslint/no-explicit-any": "off", // Disallow usage of the `any` type
    "@typescript-eslint/no-extraneous-class": "error", // Forbids the use of classes as namespaces
    "@typescript-eslint/no-floating-promises": "error", // Requires Promise-like values to be handled appropriately
    "@typescript-eslint/no-for-in-array": "error", // Disallow iterating over an array with a for-in loop
    "@typescript-eslint/no-misused-promises": [
        "error",
        { checksVoidReturn: false },
    ], // Avoid using promises in places not designed to handle them
    "@typescript-eslint/no-parameter-properties": "error", // Disallow the use of parameter properties in class constructors
    "@typescript-eslint/no-require-imports": "error", // Disallows invocation of `require()`
    "@typescript-eslint/no-unnecessary-condition": "off", // Prevents conditionals where the type is always truthy or always falsy
    "@typescript-eslint/no-unnecessary-type-arguments": "error", // Enforces that types will not to be used
    "@typescript-eslint/no-unnecessary-type-constraint": "error", // Disallows unnecessary constraints on generic types
    "@typescript-eslint/no-useless-constructor": "error", // Disallow unnecessary constructors
    "@typescript-eslint/no-use-before-define": "off", // Disallow the use of variables before they are defined
    "@typescript-eslint/prefer-for-of": "error", // Prefer a ‘for-of’ loop over a standard ‘for’ loop if the index is only used to access the array being iterated
    "@typescript-eslint/prefer-readonly": "error", // Requires that private members are marked as `readonly` if they're never modified outside of the constructor
    "@typescript-eslint/promise-function-async": "error", // Requires any function or method that returns a Promise to be marked async
};

const suggestedRules = {
    camelcase: "error", // enforce camelcase naming convention
    curly: "error", // enforce consistent brace style for all control statements
    eqeqeq: ["error", "always", {"null": "ignore"}], // require the use of `===` and `!==` except when comparing with the null literal
    "id-length": [
        "error",
        {
            min: 2,
            exceptions: ["_"],
        },
    ], // enforce minimum and maximum identifier lengths
    "max-depth": [2, 4], // enforce a maximum depth that blocks can be nested
    "max-lines": "off", // enforce a maximum number of lines per file
    "max-lines-per-function": [
        "error",
        {
            max: 100,
            skipBlankLines: true,
            skipComments: true,
        },
    ], // enforce a maximum number of lines of code in a function
    "no-array-constructor": "error", // disallow `Array` constructors
    "no-console": ["warn", { "allow": ["warn", "error"] }], // disallow the use of console
    "no-else-return": "error", // disallow `else` blocks after `return` statements in `if` statements
    "no-empty-function": "error", // disallow empty functions
    "no-invalid-this": "error", // disallow use of `this` in contexts where the value of `this` is `undefined`
    "no-lonely-if": "off", // disallow `if` statements as the only statement in `else` blocks
    "no-negated-condition": "error", // disallow negated conditions
    "no-nested-ternary": "error", // disallow nested ternary expressions
    "no-new-object": "error", // disallow `Object` constructors
    "no-param-reassign": "error", // disallow reassigning `function` parameters
    "no-plusplus": "error", // disallow the unary operators `++` and `--`
    "no-return-await": "error", // disallow unnecessary `return await`
    "no-throw-literal": "error", // disallow throwing literals as exceptions
    "no-unused-expressions": "error", // disallow unused expressions
    "no-useless-call": "error", // disallow unnecessary calls to `.call()` and `.apply()`
    "no-useless-return": "error", // disallow redundant return statements
    "no-var": "error", // require `let` or `const` instead of `var`
    "no-void": ["error", { allowAsStatement: true }], // Disallows use of the void operator.
    "prefer-const": "error", // require `const` declarations for variables that are never reassigned after declared
    "quote-props": ["error", "as-needed"], // require quotes around object literal property names
    "require-await": "off", // disallow async functions which have no `await` expression
    "spaced-comment": ["error", "always"], // enforce consistent spacing after the `//` or `/*` in a comment
};

module.exports = {
    parser: "@typescript-eslint/parser",
    plugins: ["@typescript-eslint", "unused-imports"],
    extends: [
        "eslint:recommended",
        "plugin:@typescript-eslint/eslint-recommended",
        "plugin:@typescript-eslint/recommended",
        "prettier" // Add this to disable ESLint rules that might conflict with Prettier
    ],
    overrides: [
        {
            files: ["*.test.ts"],
            rules: {
                "max-lines": "off",
                "max-lines-per-function": "off",
                "no-unused-expressions": "off",
            },
        },
    ],
    rules: {
        ...typescriptEslintRules,
        ...possibleProblemsRules,
        ...unusedImportsRules,
        ...suggestedRules,
    },
};
